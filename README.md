# Employee Management

Java Spring MVC web application 

主要功能為員工資料的新增、查詢、修改、刪除

並加入登入驗證，並以不同權限的身分開放部分特定功能

例如刪改Employee功能

## Getting Started

運行環境

* MySQL
    * port : 3306
    * account : root
    * password : 123456
    * Database : devdb    
* IDE : Intellij  
* 認證
  * 登入資訊與預設身分(帳號密碼相同)
    * ADMIN : userAdmin
    * CREATOR : userCreator
    * EDITOR : userEditor
    * VIEWER : userViewer
  * 可自行新增帳號密碼

### 其他

執行時會產生樣本資料

每次重新執行時，會更新table, data

持續練習中，可能不定時會更動

