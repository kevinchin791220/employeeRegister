# FROM ubuntu:18.04
FROM openjdk:15
MAINTAINER kevinchin

ADD build/libs/JavaSpringAdmin-0.0.1-SNAPSHOT.jar JavaSpringAdmin-0.0.1-SNAPSHOT.jar
# WORKDIR /usr/JavaSpringWorkspace
# ADD ../openjdk-15.0.2_linux-x64_bin.tar.gz /opt/Java
# RUN apt-get update -y
# add-apt-repository指令前需要安裝的
# && apt-get install software-properties-common -y \
# && add-apt-repository ppa:linuxuprising/java \

# JDK installation
# && echo openjdk-15-jdk shared/accepted-oracle-license-v1-2 select true | debconf-set-selection \
# && apt-get install openjdk-15-jdk -y
# && echo oracle-java15-installer shared/accepted-oracle-license-v1-2 select true | debconf-set-selection \
# && apt-get install oracle-java15-installer -y

ENV JAVA_HOME=/opt/Java/jdk-15.0.2
ENV PATH=$PATH:/${JAVA_HOME}/bin

# Gradle installation
# ARG GRADLE-VERSION
# ENV GRADLE-VERSION=7.0.1
RUN apt-get update -y \
&& apt-get install wget -y \
&& apt-get install unzip -y \
&& wget https://services.gradle.org/distributions/gradle-7.0.1-bin.zip -P /tmp \
&& unzip -d /opt/gradle /tmp/gradle-7.0.1-bin.zip \
&& ln -s /opt/gradle/gradle-7.0.1 /opt/gradle/latest
ENV GRADLE_HOME=/opt/gradle/latest
ENV PATH=${GRADLE_HOME}/bin:${PATH}
# RUN chmod +x /etc/profile.d/gradle.sh \
# && source /etc/profile.d/gradle.sh

# npm installation
# RUN apt-get install npm -y

# Build Project
# ./gradle bootBuildImage
# RUN cd employeeRegister
# RUN gradle build \
# && java -jar build/libs/JavaSpringAdmin-0.0.1-SNAPSHOT.jar

EXPOSE 8081
ENTRYPOINT ["java", "-jar", "JavaSpringAdmin-0.0.1-SNAPSHOT.jar"]