
import Vue from "vue";
import Router from "vue-router";
// import EmployeesList from "./components/EmployeesList.vue";
// import AddEmployee from "./components/AddEmployee.vue";
// import SearchEmployee from "./components/SearchEmployee.vue";
// import Employee from "./components/Employee.vue";
import EmployeeCRUD from './components/EmployeeCRUD.vue'
// import HelloWorld from './components/HelloWorld.vue'

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/employees",
      name: "employees",
      alias: "/employees",
    //   component: EmployeesList,
    //   children: [
    //     {
    //       path: "/employees/:id",
    //       name: "employees-details",
    //       component: Employee,
    //       props: true
    //     }
    //   ]
    },
    // {
    //   path: "/add",
    //   name: "add",
    //   component: AddEmployee
    // },
    // {
    //   path: "/search",
    //   name: "search",
    //   component: SearchEmployee
    // },
    // {
    //   path: "/hello",
    //   name: "hello",
    //   component: HelloWorld
    // },
    {
      path: "/EmployeeCRUD",
      name: "EmployeeCRUD",
      component: EmployeeCRUD
    }
  ]
});
