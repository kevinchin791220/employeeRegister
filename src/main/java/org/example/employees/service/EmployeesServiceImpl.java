package org.example.employees.service;

import org.example.employees.model.Employees;
import org.example.employees.model.ProfessionGroup;
import org.example.employees.repository.EmployeesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

// if there is no @Component, would show Error :
// "Consider defining a bean of type 'org.example.restfulapi.EmployeesServiceImpl' in your configuration."
@Component
public class EmployeesServiceImpl implements EmployeesService {

    @Autowired
    private EmployeesRepository employeesRepository;

    @Override
    public List<Employees> queryAllEmployees(){
        return employeesRepository.findAll();
    }

    @Override
    public Collection<Employees> queryEmployeesByName(String name) {
        return employeesRepository.findByName(name);
    }

    @Override
    public List<Employees> queryEmployeesBySQL() {
        return employeesRepository.findEmployeesBySQL();
    }

    @Override
    public List<Employees> queryByOrderByIdDesc(){
        return employeesRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }

    @Override
    public Long employeesCount(){
        // 取CrudRepository中的count()直接用即可，不需要跟上面的一樣在Service層寫要執行的動作
        // 一行就解決
        // 在Repository宣告.findCount()會錯誤
        return employeesRepository.count();
    }

    @Override
    public ArrayList<ProfessionGroup> queryCountGroupByProfession() {
        return employeesRepository.findCountGroupByProfession();
    }

    @Override
    public void saveEmployees(Employees employees) {
        employeesRepository.save(employees);
    }

    @Override
    public Employees getEmployees(int Id) {
        return employeesRepository.findById(Id).get();
        // return by Collection<Employee>
//        return employeesRepository.findByid(Id);
    }

    @Override
    public void updateEmployees(Employees updatedEmployee) {
        updatedEmployee.setId(updatedEmployee.getId());
        saveEmployees(updatedEmployee);
    }


    @Override
    public void deleteEmployees(int Id) {
        employeesRepository.deleteById(Id);
    }


    @Override
    public Page<Employees> queryPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
        /** 用Pageable搭配findAll()達到分頁效果 **/

        /* condition : Sort.Direction.ASC.name()
         * 降冪或升冪 */
        Sort sort = sortDirection.equalsIgnoreCase(
                Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
        return employeesRepository.findAll(pageable);
    }

    // for swagger
    @Override
    public Page<Employees> queryAllEmployeesWithPagination(Pageable pageable) {
        return employeesRepository.findAllEmployeesWithPagination(pageable);
    }

    @Override
    public List<Employees> queryEmployeesByJPQL() {
        return employeesRepository.findEmployeesByJPQL();
    }

    @Override
    public List<Employees> queryByNameLike(String name) {
//        Sort sort = sortDirection.equalsIgnoreCase(
//                Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() : Sort.by(sortField).descending();
//
//        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
        return employeesRepository.findEmployeesLike(name);
    }
}
