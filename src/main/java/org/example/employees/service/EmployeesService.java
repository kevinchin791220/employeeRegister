package org.example.employees.service;

import org.example.employees.model.Employees;
import org.example.employees.model.ProfessionGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
public interface EmployeesService {

    List<Employees> queryAllEmployees();

    Collection<Employees> queryEmployeesByName(String name);

    List<Employees> queryEmployeesBySQL();

    List<Employees> queryByOrderByIdDesc();

    ArrayList<ProfessionGroup> queryCountGroupByProfession();

    List<Employees> queryEmployeesByJPQL();

    List<Employees> queryByNameLike(String name);

    Page<Employees> queryPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

    Page<Employees> queryAllEmployeesWithPagination(Pageable pageable);

    Long employeesCount();

    void saveEmployees(Employees employees);

    Employees getEmployees(int Id);

    void updateEmployees(Employees employees);

    void deleteEmployees(int Id);
}
