package org.example.employees;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "All details about the User ")
public class EmployeesRequestDto {
    @ApiModelProperty(notes = "The database generated User age",position=1,value = "id",example="7")
    public int id;
    @ApiModelProperty(notes = "The database generated User name",position=2,value = "name",example="Kevin")
    public String name;
    @ApiModelProperty(notes = "The database generated User phone",position=3,value = "phoneNum",example="0975123654")
    public String phoneNum;
}
