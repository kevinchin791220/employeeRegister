package org.example.employees.controller;

import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.example.employees.model.Employees;
import org.example.employees.model.ProfessionResponse;
import org.example.employees.service.EmployeesServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class GraphqlController {

    @Autowired
    EmployeesServiceImpl employeesServiceImpl;

    @Value("classpath:employee.graphqls")
    private Resource schemaResource;

    private GraphQL graphQL;

    public void loadSchema() throws IOException {
        File schemaFile = schemaResource.getFile();
        TypeDefinitionRegistry registry = new SchemaParser().parse(schemaFile);
        RuntimeWiring wiring = buildWiring();
        GraphQLSchema graphQLSchema = new SchemaGenerator().makeExecutableSchema(registry, wiring);
        graphQL = GraphQL.newGraphQL(graphQLSchema).build();
    }

    private RuntimeWiring buildWiring() {
        DataFetcher<List<Employees>> fetcherAllEmployees = dataFetchingEnvironment -> {
            return (List<Employees>) employeesServiceImpl.queryAllEmployees();
        };

        DataFetcher<Employees> fetcherEmployee = dataFetchingEnvironment -> {
            return (Employees) employeesServiceImpl.getEmployees(dataFetchingEnvironment.getArgument("id"));
        };

        return RuntimeWiring.newRuntimeWiring().type("Query", typeWriting ->
            typeWriting.dataFetcher("getAllEmployees", fetcherAllEmployees)
                    .dataFetcher("getEmployee", fetcherEmployee)
        ).build();
    }

    @GetMapping("/graphGetAllEmployees")
    public ResponseEntity<Object> graphGetAllEmployees(@RequestBody String query) {
        ExecutionResult result = graphQL.execute(query);
        return new ResponseEntity<Object>(result, HttpStatus.OK);
    }

    @GetMapping("/graphGetEmployeesById")
    public ResponseEntity<Object> graphGetEmployeesById(@RequestBody String query) {
        ExecutionResult result = graphQL.execute(query);
        return new ResponseEntity<Object>(result, HttpStatus.OK);
    }

}
