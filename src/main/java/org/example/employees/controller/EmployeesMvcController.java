package org.example.employees.controller;

import io.swagger.annotations.Api;
import org.example.employees.model.Professions;
import org.example.employees.service.EmployeesServiceImpl;
import org.example.employees.model.Employees;
import org.example.employees.repository.EmployeesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//import javax.validation.Valid;

@Api(tags="EmployeesMvcController",description="類別檔案說明")
@Component
@Controller
public class EmployeesMvcController {
    int pageDataSize = 5;
    List<Professions> listProfession = Arrays.asList(Professions.values());
    @Autowired
    EmployeesServiceImpl employeesServiceImpl;

    @Autowired
    EmployeesRepository employeesRepository;

    // CRUD
    @GetMapping(value = "/signup")
    public String showSignUpForm(Employees employees, Model model) {
        // 注入profession

        model.addAttribute("listProfession", listProfession);
        return "employee/add-Employee";
    }

    @PostMapping(value = "/addEmployee")
    public String addEmployee(Employees employees, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "employee/add-employee";
        }

        employeesServiceImpl.saveEmployees(employees);
        return "redirect:/index";
    }

    @GetMapping("/index")
    public String viewHomePage(Model model) {
        return showEmployeeList(1, "Name", "asc", model);
    }

    @GetMapping("/index/{pageNo}")
    public String showEmployeeList(@PathVariable (value = "pageNo") int pageNo,
                               @RequestParam("sortField") String sortField,
                               @RequestParam("sortDir") String sortDir,
                               Model model) {

        Page<Employees> page = employeesServiceImpl.queryPaginated(pageNo, pageDataSize, sortField, sortDir);

        model.addAttribute("title", "List of Employee");
        model.addAttribute("isBySearch", false);
        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("employees", page.getContent());
//        List<Employees>
//        model.addAttribute("employees", employeesServiceImpl.queryAllEmployees());
        return "employee/index";
    }

    @PostMapping("/search")
    public String viewSearchResult(@RequestParam ("search") String search,
                                   Model model) {
        /** note:
         * HTML :
         * 1. method=POST
         * 2. th:action="@{/search}<這裡不用加parameter>"
         *
         * Controller :
         * 按下submit後，URL中就有?search=<> (th:name="search" 如果名稱不是search => error, why?)
         * 在Controller端要用@RequestParam接收
         * **/
//        List<Employees> page = employeesServiceImpl.queryByNameLike(search);
        model.addAttribute("title", "Search result of Employee");
        model.addAttribute("isBySearch", true);
        model.addAttribute("employees", employeesServiceImpl.queryByNameLike(search));
        return "employee/index";
//        return searchEmployee(1, "Name", "asc", search, model);
    }

//    @GetMapping("/search/{pageNo}")
//    public String searchEmployee(@PathVariable (value = "pageNo") int pageNo,
//                                 @RequestParam("sortField") String sortField,
//                                 @RequestParam("sortDir") String sortDir, String search, Model model) {

//        model.addAttribute("currentPage", pageNo);
//        model.addAttribute("totalPages", page.getTotalPages());
//        model.addAttribute("totalItems", page.getTotalElements());
//
//        model.addAttribute("sortField", sortField);
//        model.addAttribute("sortDir", sortDir);
//        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

//    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
//        Employees employee = employeesServiceImpl.getEmployees(id)
//                .orElseThrow(() -> new IllegalArgumentException("Invalid employee ID : " + id));
        // 注入employees
        Employees employee = employeesServiceImpl.getEmployees(id);
        /* attributeName <=> th:object="${}"
            "employees"  <=> "${employees}" (should be the same object name!!!)
         */
        model.addAttribute("employee", employee);

        // 注入profession
        model.addAttribute("listProfession", listProfession);

        // 若未設定Getter, Setter，對應到html會錯誤 (Ex : *{profession})
        return "employee/update-employee";
    }

    // run on browser, so change PUT to POST
    // @PathVariable("id") long id => not necessary(?)
    // 如果url沒有{id}，執行後會多一筆結果(why?)
    @PostMapping(value = "/update/{id}")
    public String updateEmployee(Employees updatedEmployees,
                                 BindingResult result,
                                 Model model) {
        if (!result.hasErrors()) {
            employeesServiceImpl.updateEmployees(updatedEmployees);
        }

        return "redirect:/index";
    }

    // run on browser, so change DELETE to GET
    @GetMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable ("id") int id, Model model)
    {
        employeesServiceImpl.deleteEmployees(id);
        // 重新導向"/index"這個連結，會由Controller負責"/index"的function()處理
        return "redirect:/index";
    }
}
