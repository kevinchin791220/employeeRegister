package org.example.employees.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.example.employees.model.*;
import org.example.employees.service.EmployeesServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Api(tags="EmployeesRestController",description="類別檔案說明")
@RestController
@ApiResponses(value = {

        @ApiResponse(code = 200, message = "Successfully retrieved list"),

        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),

        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")

}) //加很多狀況
public class EmployeesRestController {
    /*
    * Why ServiceImpl?
    * 降低耦合
    * Ex : 假設需要更換DB的時候 會便利很多(這例子對嗎?)
    * */
//    @Autowired
//    EmployeesRepository employeesRepository;

//    @Autowired
//    EmployeesService employeesService;

    @Autowired
    EmployeesServiceImpl employeesServiceImpl;

    @ApiOperation(value="Get All data of Employees", notes="取得所有Employees table資料")
    @GetMapping(value = "/query-all-employees")
    List<Employees> queryAllEmployees() {
        return employeesServiceImpl.queryAllEmployees();
    }

    @ApiOperation(value="query Employees By SQL", notes="query Employees By SQL")
    @GetMapping(value = "/query-employees-by-sql")
    List<Employees> queryEmployeesBySQL(){
        return employeesServiceImpl.queryEmployeesBySQL();
    }

    @ApiOperation(value="show data in DESC", notes="降冪顯示資料")
    @GetMapping(value = "/query-id-desc")
    List<Employees> queryByOrderByIdDesc(){
        return employeesServiceImpl.queryByOrderByIdDesc();
    }

    @ApiOperation(value="show count of Employees", notes="取CrudRepository中的count()取得總數")
    @GetMapping(value = "/employees-count")
    Long employeesCount(){
        return employeesServiceImpl.employeesCount();
    }

    @ApiOperation(value="query data by Name", notes="query data by Name")
    @GetMapping(value = "/query-employees-by-name/{name}")
    @ResponseBody
    public Collection<Employees> queryEmployeesByName(@PathVariable String name) {
        return employeesServiceImpl.queryEmployeesByName(name);
    }

    @GetMapping(value = "/query-employees-by-id/{id}")
    @ResponseBody
    public Employees employeesById(@PathVariable int id) {
        return employeesServiceImpl.getEmployees(id);
    }

    @ApiOperation(value="add employee data", notes="add new employee data")
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity add(@RequestBody Employees employees) {
        try
        {
            employeesServiceImpl.saveEmployees(employees);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body("Success");
        }
        catch (Exception e)
        {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Failed");
        }
    }
    @ApiOperation(value="update employees data", notes="修改員工資訊")
    @PutMapping(value = "/update/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody Employees employees) {
        try
        {
            employees.setId(employees.getId());
            employeesServiceImpl.saveEmployees(employees);

            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body("Success");
        }
        catch (Exception e)
        {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Failed");
        }
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        try
        {
            employeesServiceImpl.deleteEmployees(id);

            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body("Success");
        }
        catch (Exception e)
        {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Failed");
        }
    }

    @GetMapping("/get-employee-data")
    public ResponseEntity<ArrayList<ProfessionResponse>> getPieChartData() {
        /***
         * 不可直接return JSONArray
         * 應該return自訂的model class搭配List物件
         * ***/
        Map<Professions, Long> graphData = new TreeMap<>();

        ArrayList<ProfessionResponse> pieChartData = new ArrayList<>();
        // Object : ArrayList
        for (ProfessionGroup professionGroup : employeesServiceImpl.queryCountGroupByProfession()) {
            try {
                ProfessionResponse data = new ProfessionResponse();
                data.name = professionGroup.getProfessions();
                data.y = professionGroup.getCount();

                pieChartData.add(data);
            }
            catch (Exception e) {

            }
        }

        return new ResponseEntity<>(pieChartData, HttpStatus.OK);
    }

    /**
     * Parameters for Pageable
     * @Param int page
     * @Param int size
     * @Param Sort sort
     * @Param int limit(?)
     * **/

    /** swagger產生的parameter與pageable實際使用的parameter不同
     * 不能直接用swagger測
     * Ex : (pageSize, size)
     * from swagger => ?offset=1&pageNumber=2&pageSize=3
     * successful url => ?page=0&size=3&sort=id,DESC**/
    @GetMapping("/get-employee-data-pageable")
    public Page<Employees> queryAllEmployeesWithPagination(Pageable pageable) {
//        pageable = PageRequest.of(3, 3);
        Page<Employees> page = employeesServiceImpl.queryAllEmployeesWithPagination(pageable);

        List<Employees> listEmployees = page.getContent();
        int totalPages = page.getTotalPages();
        Long totalElements = page.getTotalElements();

        return employeesServiceImpl.queryAllEmployeesWithPagination(pageable);
    }

    @GetMapping("/get-employee-data-jpql")
    public List<Employees> queryEmployeesByJPQL() {

        return employeesServiceImpl.queryEmployeesByJPQL();
    }

}