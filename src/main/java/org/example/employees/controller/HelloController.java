//package org.example.employees.controller;
//
//import org.junit.jupiter.api.Test;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//public class HelloController {
//    // for JUnit Test trial
//    @GetMapping("/hello")
//    public String hello(@RequestParam(name = "name", defaultValue = "World") String name) {
//        return String.format("Hello, %s", name);
//    }
//
//}
