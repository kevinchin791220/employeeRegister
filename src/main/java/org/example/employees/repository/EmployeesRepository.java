package org.example.employees.repository;

import org.example.employees.model.Employees;
import org.example.employees.model.ProfessionGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

//@Transactional(readOnly = true)
//@EnableJpaRepositories("org.example.restfulapi")
@Component
//@ImportResource("classpath:jpa-config.xml")
//@NoRepositoryBean
public interface EmployeesRepository extends JpaRepository<Employees, Integer> {
    // naming rule reference : https://www.baeldung.com/spring-data-derived-queries
    // find + By + fieldName
    // fieldName : 在這裡要大寫開頭 在Class設定要小寫
    Collection<Employees> findByName(String name);
    // error
    // findById 是CrudRepository的function
    // 但在這邊用findById這個名稱的話 會優先使用這邊的
    // 所以需要把Table名稱也改掉 做出區隔
    Collection<Employees> findByid(int Id);

    // native query = true : 用該資料庫的原始語法查詢(for MySQL here)
    // native query = false : 用JPQL查詢 可對應到其他資料庫
    @Query(value = "SELECT " +
                    "   e.id, e.name, e.phone_num, e.profession " +
                    "FROM " +
                    "   devdb.employees e",
                    nativeQuery = true)
    List<Employees> findEmployeesBySQL();

    // JPQL 可以不用在SELECT處列出所有欄位
    @Query(value = "SELECT " +
                    "   e " +
                    "FROM " +
                    "   Employees e",
                    nativeQuery = false)
    List<Employees> findEmployeesByJPQL();

    /* 跟native query不同處(以model物件名稱為主 ())
    * FROM employees -> Employees (checked)*/
    @Query(value = "SELECT " +
                    "   new org.example.employees.model.ProfessionGroup(e.profession, COUNT(e)) " +
                    "FROM " +
                    "   Employees e " +
                    "GROUP BY " +
                    "   e.profession",
            nativeQuery = false)
    ArrayList<ProfessionGroup> findCountGroupByProfession();

    @Query(value = "SELECT " +
                    "   e " +
                    "FROM " +
                    "   Employees e",
                    nativeQuery = false)
    Page<Employees> findAllEmployeesWithPagination(Pageable pageable);

    @Query(value = "SELECT " +
                    "   e " +
                    "FROM " +
                    "   Employees e " +
                    "WHERE " +
                    "   e.name LIKE CONCAT('%',:name ,'%')",
                    nativeQuery = false)
    List<Employees> findEmployeesLike(@Param("name") String name);

//    List<Employees> findByOrderByidDesc();

//    Long findCount();

//    userRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));

//    userRepository.findAll(Sort.by("LENGTH(name)"));
}