package org.example.employees;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
public class StaticResource extends WebMvcConfigurerAdapter {

    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        /*
         root : static
         /** => static/
         /css => static/css
         */

        registry
                .addResourceHandler("/**")
                .addResourceLocations("classpath:/static/")
//                .addResourceLocations("classpath:/resource/")
//                .addResourceLocations("classpath:/static/css/employeeStyle/*")
                .addResourceLocations("classpath:/static/css/employee/*")
                .addResourceLocations("classpath:/static/image/*")
                .addResourceLocations("classpath:/static/js/employee/*");
    }
}