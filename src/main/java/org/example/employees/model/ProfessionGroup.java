package org.example.employees.model;

public class ProfessionGroup {
    public Professions professions;
    public Long count;

    public ProfessionGroup(Professions professions, Long count) {
        this.professions = professions;
        this.count = count;
    }

    public Professions getProfessions() {
        return professions;
    }

    public void setProfessions(Professions professions) {
        this.professions = professions;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
