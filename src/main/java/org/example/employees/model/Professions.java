package org.example.employees.model;

public enum Professions {
    Developer,
    Tester,
    Architect;
}
