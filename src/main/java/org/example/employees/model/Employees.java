package org.example.employees.model;

import javax.persistence.*;

@Entity
@Table(name = "employees")
public class Employees {
//    @NotEmpty(message="not null")
    @Id
    // 有這行，等於是在create table時帶入AUTO_INCREMENT指令
    // 這樣寫入資料時就算沒有提供id也可以寫入
    // Ex : id int NOT NULL AUTO_INCREMENT
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id", columnDefinition="Number(10) default '34562'")
    @Column(name = "id")
    public int id;

    @Column(name = "name")
    public String name;

    @Column(name = "phone_num")
    public String phoneNum;

    @Column(name = "profession")
    @Enumerated(EnumType.STRING)
    public Professions profession;

    public Employees() {
    }

    public Employees(String name, String phoneNum, Professions profession) {
        this.name = name;
        this.phoneNum = phoneNum;
        this.profession = profession;
    }

    //    @OneToMany

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public Professions getProfession() {
        return profession;
    }

    public void setProfession(Professions profession) {
        this.profession = profession;
    }

    // for batch practice
    @Override
    public String toString() {
        return "name: " + name + ", phone_num: " + phoneNum + ", profession:" + profession;
    }
}