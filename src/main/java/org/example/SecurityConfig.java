package org.example;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.example.auth.errorhandling.CustomAuthenticationFailureHandler;
import org.example.auth.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
// By adding @EnableWebSecurity, we get Spring Security and MVC integration support:
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Qualifier("userDetailsServiceImpl")

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // authentication manager
        auth.userDetailsService(userDetailsService);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

//    @Bean
//    public DelegatingPasswordEncoder delegatingPasswordEncoder() {
//        return new DelegatingPasswordEncoder();
//    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(bCryptPasswordEncoder());

        return authProvider;
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.authenticationProvider(authenticationProvider());
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // http builder configurations for authorize requests and form login
        // for Spring Admin
        http
                // 關閉對 CSRF（跨站請求偽造）攻擊的防護。
                // 這樣 Security 機制才不會拒絕外部直接對 API 發出的請求，如 Postman 與前端。
                .csrf().disable()
                .authorizeRequests()
                /*  「*」：代表0到多個字元。如「/products/*」適用於「/products」、「/products/123」，但不適用「/products/123/draft」
                    「**」：代表0到多個路徑。如「/products/**」適用於「/products」底下任何路徑。
                    「?」：代表一個字元。如「/products/?*」適用於「/products/1」、「/products/123」，但不適用「/products」。*/
                    .antMatchers("/user-management/").hasRole("ADMIN")
                    .antMatchers("/edit/{id}/").hasAnyRole("ADMIN", "CREATOR", "EDITOR")
                // 後面加入for Spring Boot Admin的, "/actuator" , "/actuator/**"
                // 以達到免登入即可取得health資訊
                // 不然admin端無法在MVC畫面進行登入，就拿不到heartBeat資料了
                    .antMatchers("**/css/**", "**/js/**", "**/image/**", "/registration", "/actuator", "/actuator/**")
                    .permitAll()
                    .antMatchers("/resources/**", "/registration", "/static/**").permitAll()
                    .antMatchers("/admin").hasRole("ADMIN")
                    .antMatchers("/user").hasAnyRole("ADMIN", "USER")
                    .antMatchers("/").permitAll()
                    .antMatchers("/index").hasAnyRole("ADMIN", "USER")
                    .antMatchers("/").permitAll()
                    // 特定身分可存取
//                    .antMatchers("/userlist").hasAuthority("ROLE_ADMIN")
//                    .antMatchers("/dcn/add").hasAuthority("ROLE_ADMIN")
                    // .anyRequest().authenticated()功能 除了上面設定過antMatchers的url以外，其他都需要登入
                    .anyRequest().authenticated()
                    .and()

                // formLogin 則是啟用內建的登入畫面
                .formLogin()
                    .loginPage("/login")
                    .defaultSuccessUrl("/index/", true)
//                    .failureUrl("/login?error=true")
//                    .failureUrl("/login?error")
                    .failureUrl("/login.html?error")
                    .failureHandler(authenticationFailureHandler())
                    .permitAll()
                    .and()
                .logout()
                    .logoutSuccessUrl("/login?logout")
                    .permitAll();
// 加了http.httpBasic(); 登入就出問題了!
        //通常不會搭配formLogin一起使用
//        http.httpBasic();
//                        .failureHandler(new AuthenticationFailureHandler() {
//                    @Override
//                    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
//                                                        AuthenticationException exception) throws IOException, ServletException {
//                        System.out.println("Login failed");
//                        System.out.println(exception);
//
//                        response.sendRedirect("/login_error");
//                    }
//                })
//                    .failureHandler(authenticationFailureHandler())
//                    .permitAll()
//        ;
        // configuration in jsp example
//        http
//                .authorizeRequests()
//                    .antMatchers("/resources/**", "/registration").permitAll()
//                    .anyRequest().authenticated()
//                    .and()
//                .formLogin()
//                    .loginPage("/login")
//                    .permitAll()
//                    .and()
//                .logout()
//                    .permitAll();
//        http
////                .exceptionHandling()
////                    .accessDeniedPage("/403")//存取被拒時導往的URL
////                .and()
//                .authorizeRequests()
//                    // 設定可存取的靜態資料(還有WebMvcConfigurerAdapter)
//                    .antMatchers("/", "/home", "/static/**").permitAll()
//                    .antMatchers("/resources/**", "/registration").permitAll()
//                    .antMatchers("/admin").hasRole("ADMIN")
//                    .antMatchers("/user").hasAnyRole("ADMIN", "USER")
//                    .antMatchers("/").permitAll()
//                    .antMatchers("/index").hasAnyRole("ADMIN", "USER")
//                    .antMatchers("/").permitAll()
//                    // 特定身分可存取
////                    .antMatchers("/userlist").hasAuthority("ROLE_ADMIN")
////                    .antMatchers("/dcn/add").hasAuthority("ROLE_ADMIN")//必須是admin才可以存取
////                    .antMatchers("/dcn/**") //對象為所有網址
////                    .anyRequest().authenticated()
//                .and()
//                .formLogin()
////                    .loginPage("/login")
////                    .usernameParameter("email")
////                    .passwordParameter("password")
////                    .loginProcessingUrl("/doLogin")
//                    .defaultSuccessUrl("/index", true)
////                    .failureUrl("/login_error")
////                    .successForwardUrl("/login_success_handler")
////                    .failureForwardUrl("/login_failure_handler")
////                    .successHandler(new AuthenticationSuccessHandler() {
//
////                        @Override
////                        public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, FilterChain chain, org.springframework.security.core.Authentication authentication) throws IOException, ServletException {
////                            System.out.println("Logged user: " + authentication.getName());
////
////                            response.sendRedirect("/index");
////                        }
////
////                        @Override
////                        public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, org.springframework.security.core.Authentication authentication) throws IOException, ServletException {
////                            System.out.println("Logged user: " + authentication.getName());
////
////                            response.sendRedirect("/index");
////                        }
////                    })
////                .failureHandler(new AuthenticationFailureHandler() {
////                    @Override
////                    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
////                                                        AuthenticationException exception) throws IOException, ServletException {
////                        System.out.println("Login failed");
////                        System.out.println(exception);
////
////                        response.sendRedirect("/login_error");
////                    }
////                })
//////            .failureHandler(authenticationFailureHandler())
////                    .permitAll()
////                .and()
////                .logout()
////                    .logoutUrl("/doLogout")
////                    .logoutSuccessUrl("/logout_success")
////                    .logoutSuccessHandler(new LogoutSuccessHandler() {
////
////                        @Override
////                        public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, org.springframework.security.core.Authentication authentication) throws IOException, ServletException {
////                            System.out.println("This user logged out: " + authentication.getName());
////
////                            response.sendRedirect("/logout_success");
////                        }
////
////                    })
//                    .permitAll()
////                .and()
////                    .cors()
////                    .and()
////                    .csrf().disable()

    }

//    @Bean
//    public PasswordEncoder getPasswordEncoder() {
//        return NoOpPasswordEncoder.getInstance();
//    }
}