package org.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;

// url : http://localhost:8081/swagger-ui/
// 注意最後面的"/"

@Configuration
@EnableSwagger2
@Import(SpringDataRestConfiguration.class)
public class SwaggerConfig {
    @Bean
    public Docket employeeRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
//                .apiInfo(new ApiInfoBuilder().version("1.0").title("Employees API").build())
                .groupName("Employee")
                .tags(new Tag("EmployeesRestController", "Employees Table的資料操作"))
                // 可增加多個Tag
//                .tags(new Tag("BookingRestController", "類別檔案說明"),new Tag("TestController", "測試檔案說明"))
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.example.employees"))
                .paths(PathSelectors.any())//paths(PathSelectors.regex("/.*"))
                .build();
    }

//    @Bean
//    public Docket userRestApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .apiInfo(apiInfo())
////                .apiInfo(new ApiInfoBuilder().version("1.0").title("User API").build())
//                .groupName("User")
//                .tags(new Tag("UserRestController", "User Table的資料操作"))
//                .select()
//                .apis(RequestHandlerSelectors.basePackage("org.example.auth"))
//                .paths(PathSelectors.any())//paths(PathSelectors.regex("/.*"))
//                .build();
//    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Kevin's Java Spring development for study")
                .description("相關說明")
                .termsOfServiceUrl("https://www.pixnet.net/pcard/B0212066/")
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .version("1.0.0")
                .build();
    }
}