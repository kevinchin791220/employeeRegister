package org.example.vueTest;

import org.example.employees.repository.EmployeesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

//@CrossOrigin(origins = "http://localhost:3000")
@RestController
//@RequestMapping("/api/messages")
public class EmployeeVueController {
//    @Autowired
//    EmployeesServiceImpl employeesServiceImpl;

    @Autowired
    EmployeesRepository employeesRepository;

    @GetMapping("/EmployeeCRUD")
    public String EmployeeCRUD() {
        return "EmployeeCRUD";
    }

    @GetMapping("/employees")
    public List getAllEmployees() {
        System.out.println("Get all employees...");

        List employees = new ArrayList<>();
        employeesRepository.findAll().forEach(employees::add);

        return employees;
    }

//    @PostMapping("/employees")
//    public Employees postCustomer(@RequestBody Employees employees) {
//
//        Employees _employees = repository.save(new Employees(employees.getName(), employees.getAge()));
//        return _customer;
//    }

//    @DeleteMapping("/customer/{id}")
//    public ResponseEntity deleteCustomer(@PathVariable("id") long id) {
//        System.out.println("Delete Customer with ID = " + id + "...");
//
//        repository.deleteById(id);
//
//        return new ResponseEntity<>("Customer has been deleted!", HttpStatus.OK);
//    }
//
//    @GetMapping("customers/age/{age}")
//    public List findByAge(@PathVariable int age) {
//
//        List customers = repository.findByAge(age);
//        return customers;
//    }
//
//    @PutMapping("/customer/{id}")
//    public ResponseEntity updateCustomer(@PathVariable("id") long id, @RequestBody Customer customer) {
//        System.out.println("Update Customer with ID = " + id + "...");
//
//        Optional customerData = repository.findById(id);
//
//        if (customerData.isPresent()) {
//            Customer _customer = customerData.get();
//            _customer.setName(customer.getName());
//            _customer.setAge(customer.getAge());
//            _customer.setActive(customer.isActive());
//            return new ResponseEntity<>(repository.save(_customer), HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//    }
}
