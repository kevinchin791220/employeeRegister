package org.example;

//import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.example.auth.model.RoleEnum;
import org.example.auth.model.User;
import org.example.auth.repository.RoleRepository;
import org.example.auth.repository.UserRepository;
import org.example.auth.service.UserServiceImpl;
import org.example.employees.model.Employees;
import org.example.employees.model.Professions;
import org.example.employees.repository.EmployeesRepository;
import org.example.employees.service.EmployeesServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

// official tutorial
@Configuration
// Tags the class as a source of bean definitions for the application context.
@ComponentScan(basePackages = {"org.example","org.example.employees","org.example.mvc","org.example.auth"})
//@ComponentScan
// Tells Spring to look for other components, configurations, and services in the com/example package, letting it find the controllers.
@EnableAutoConfiguration
// Tells Spring Boot to start adding beans based on classpath settings, other beans, and various property settings. For example, if spring-webmvc is on the classpath, this annotation flags the application as a web application and activates key behaviors, such as setting up a DispatcherServlet
@SpringBootApplication
//@ServletComponentScan
@EnableJpaRepositories(basePackageClasses =
        {
                UserRepository.class,
                EmployeesRepository.class
        })
//@ImportResource("classpath:/applicationContext.xml")
//@EnableJpaRepositories
public class Application implements CommandLineRunner, WebMvcConfigurer {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private EmployeesServiceImpl employeesServiceImpl;

    public static void main(String args[])
    {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Spring boot is running ...");

//        User userKevin = new User();
//        userKevin.setUsername("kevinchin");
//        userKevin.setPassword("kevinchin");
//        userServiceImpl.addUserRole(userKevin, RoleEnum.ADMIN);
//        System.out.println("ADMIN CREATED ...");

//        User userSimon = new User();
//        userSimon.setUsername("simonchen");
//        userSimon.setPassword("simonchen");
//        userServiceImpl.addUserRole(userSimon, RoleEnum.CREATOR);
//        System.out.println("CREATOR CREATED ...");
//
//        User userAdmin = new User();
//        userAdmin.setUsername("userAdmin");
//        userAdmin.setPassword("userAdmin");
//        userServiceImpl.addUserRole(userAdmin, RoleEnum.ADMIN);
//        System.out.println("userAdmin CREATED ...");
//
//        User userCreator = new User();
//        userCreator.setUsername("userCreator");
//        userCreator.setPassword("userCreator");
//        userServiceImpl.addUserRole(userCreator, RoleEnum.CREATOR);
//        System.out.println("userCreator CREATED ...");
//
//        User userEditor = new User();
//        userEditor.setUsername("userEditor");
//        userEditor.setPassword("userEditor");
//        userServiceImpl.addUserRole(userEditor, RoleEnum.EDITOR);
//        System.out.println("userEditor CREATED ...");
//
//        User userViewer = new User();
//        userViewer.setUsername("userViewer");
//        userViewer.setPassword("userViewer");
//        userServiceImpl.addUserRole(userViewer, RoleEnum.VIEWER);
//        System.out.println("userViewer CREATED ...");

//        saveNewEmployee(1, "Jesse", "0987654321", Professions.Architect);
//        saveNewEmployee(2, "Thomas", "0987788778", Professions.Architect);
//        saveNewEmployee(3, "Kevin", "0987788779", Professions.Developer);
//        saveNewEmployee(4, "Fiona", "0987785668", Professions.Developer);
//        saveNewEmployee(5, "Phoebe", "0987788899", Professions.Tester);
//        saveNewEmployee(6, "Winnie", "0986688899", Professions.Developer);
//        saveNewEmployee(7, "Mark", "0986681899", Professions.Developer);
//        saveNewEmployee(8, "Simon", "0951682899", Professions.Developer);
//        saveNewEmployee(9, "Trista", "0951712899", Professions.Tester);
//        saveNewEmployee(10, "Johnny", "0951731299", Professions.Tester);
//        saveNewEmployee(11, "Abe", "0951733299", Professions.Tester);

        System.out.println("All Employee Data CREATED");
        System.out.println("for test : kevinchin");
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/logout_success").setViewName("logout_success");
        registry.addViewController("/chart").setViewName("employee/chart");
    }
    private boolean enumContainsValue(String value)
    {
        for (RoleEnum roleEnum : RoleEnum.values())
        {
            if (roleEnum.name().equals(value))
            {
                return true;
            }
        }

        return false;
    }

    private void saveNewEmployee(int id, String name, String phoneNum, Professions profession) {
        Employees employees = new Employees();
        employees.setId(id);
        employees.setName(name);
        employees.setPhoneNum(phoneNum);
        employees.setProfession(profession);
        employeesServiceImpl.saveEmployees(employees);
        System.out.println("Employee " + name + " CREATED");
    }
}

