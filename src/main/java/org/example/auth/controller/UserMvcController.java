package org.example.auth.controller;

import org.example.auth.model.Role;
import org.example.auth.model.RoleEnum;
import org.example.auth.model.User;
import org.example.auth.model.UserUpdate;
import org.example.auth.service.SecurityService;
import org.example.auth.service.UserServiceImpl;
import org.example.auth.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Component
@Controller
public class UserMvcController {

//    @Autowired
//    private UserService userService;

    @Autowired
    private UserServiceImpl userServiceImpl;

//    @Autowired
//    private RoleServiceImpl roleServiceImpl;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;


    @GetMapping("/registration")
    public String registration(Model model) {
        // Service to ServiceImpl
        if (securityService.isAuthenticated()) {
            return "redirect:/";
        }
        List<RoleEnum> listRole = Arrays.asList(RoleEnum.values());
        model.addAttribute("userForm", new User());


        model.addAttribute("listRole", listRole);

        return "auth/registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") User userForm,
                               BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "auth/registration";
        }

        userServiceImpl.addUserRole(userForm, RoleEnum.VIEWER);

        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/index";
    }

    // We don't define /login POST controller, it is provided by Spring Security
    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (securityService.isAuthenticated()) {
            return "redirect:/index";
        }

        if (error != null)
            model.addAttribute("error",
                    "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message",
                    "You have been logged out successfully.");

        return "auth/login";
    }

    @GetMapping("/user-management")
    public String showUserManagement(Model model)
    {
        /***
         * th:selected無法與th:field混用
         * http://forum.thymeleaf.org/form-select-th-field-and-th-selected-not-working-how-do-you-set-default-value-td4030583.html
         * ***/
        List<User> allUsers = userServiceImpl.queryAllUserRole();
        model.addAttribute("userManagementList", allUsers);
        return "auth/user-management";
    }

    @GetMapping(value = "/editUserInfo/{id}")
    public String showUpdateUserForm(@PathVariable("id") int id,
                                    Model model) {
        /***
         * 目前無法將User內的Set<Role>物件使用於th:selected
         * 已嘗試作法
         * 1. 利用div加入其他th:object***/
        UserUpdate updatedUser = userServiceImpl.getUserInfo(id);

        /* attributeName <=> th:object="${}"
            "employees"  <=> "${employees}" (should be the same object name!!!)
         */
        model.addAttribute("updatedUser", updatedUser);
        return "auth/update-user";
    }

    @PostMapping("/updateUser/{id}")
    public String updateUser(@ModelAttribute("updatedUser") UserUpdate updatedUser,
                             BindingResult result,
                             Model model) {

        if (!result.hasErrors()) {
            userServiceImpl.updateUserRole(updatedUser, updatedUser.getRoleEnum());
        }
        if (result.hasErrors()) {
            System.out.println("There is an Error!");
            // 以後改成導向原本畫面(update-user) 並且提示錯誤訊息(<span>)
            return "auth/user-management";
        }

        return "redirect:/user-management";
    }

    @GetMapping(value = "/updateActive/{id}")
    public String updateActive(@PathVariable int id) {
        // 先取得目前資料中isActive欄位的狀態
        // 切換狀態後 update

        User user = userServiceImpl.getUser(id);
        if (user.isActive()) {
            user.setActive(false);
        }
        else {
            user.setActive(true);
        }
        user.setId(user.getId());
        userServiceImpl.saveUser(user);

        return "redirect:/user-management";

    }
}

