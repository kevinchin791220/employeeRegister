package org.example.auth.service;

public interface SecurityService {

    boolean isAuthenticated();

    void autoLogin(String username, String password);
}
