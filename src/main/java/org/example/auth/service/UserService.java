package org.example.auth.service;

import org.example.auth.model.Role;
import org.example.auth.model.RoleEnum;
import org.example.auth.model.User;
import org.example.auth.model.UserUpdate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    void addUserRole(User user, RoleEnum roleEnum);

    void updateUserRole(UserUpdate userUpdate, RoleEnum role);

    User findByUsername(String username);

    User getUser(int Id);

    UserUpdate getUserInfo(int Id);

    List<User> queryAllUserRole();

    void saveUser(User user);
}
