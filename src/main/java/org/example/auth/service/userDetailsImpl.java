package org.example.auth.service;

import org.example.auth.model.Role;
import org.example.auth.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

public class userDetailsImpl implements UserDetails {
    private String ROLE_PREFIX = "ROLE_";

    private User user;
    private Set<Role> roles;

    private String username;
    private String password;
    private boolean isActive;
    private String role;
    private ArrayList<GrantedAuthority> authorities;

    public userDetailsImpl(User user) {
        this.user = user;
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.isActive = user.isActive();
        this.roles = user.getRoles();
        for (Role role : roles) {
            this.role = role.getRoleName().toString();
//            this.authorities = Arrays.stream(role.getName())
//                    .map(SimpleGrantedAuthority::new)
//                    .collect(Collectors.toList());
        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        authorities = new ArrayList<>();

        for (Role role : roles) {
            role.getRoleName();
            authorities.add(new SimpleGrantedAuthority(role.getRoleName().toString()));
//            authorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + role));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActive;
    }
}
