package org.example.auth.service;

import org.example.auth.model.Role;
import org.example.auth.model.RoleEnum;
import org.example.auth.model.User;
import org.example.auth.model.UserUpdate;
import org.example.auth.repository.RoleRepository;
import org.example.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
//    @Autowired
//    private DelegatingPasswordEncoder delegatingPasswordEncoder;

    @Override
    public void addUserRole(User user, RoleEnum roleName) {

        // 經過判斷role是否存在後，將得出結果的role加入Set物件中
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(addRole(user, roleName));

        // 對密碼加密、設定ID、將Set<Role>物件加入user物件中
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(roleSet);
        user.setActive(true);

        userRepository.save(user);
//        userRepository.saveAndFlush(user);
    }

    @Override
    public void updateUserRole(UserUpdate userUpdate, RoleEnum updatedRole) {

        // 取得被修改對象的資料
        User user = userRepository.findById(userUpdate.getId()).get();

        Set<Role> roleSet = new HashSet<>();
        roleSet.add(addRole(user, updatedRole));

        user.setId(userUpdate.getId());
        // 將修改內容加入user物件中
        user.setRoles(roleSet);
//        user.setActive(userEdit.isActive());

        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User getUser(int Id) {
        return userRepository.findById(Id).get();
    }

    @Override
    public UserUpdate getUserInfo(int Id) {
        return userRepository.findUserInfoById(Id);
//        return null;
    }

    @Override
    public List<User> queryAllUserRole() {
        return userRepository.findAll();
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

    private Role addRole(User user, RoleEnum roleName) {
        Role newRole = new Role();

        if (roleRepository.findAll().size() == 0) {
            newRole.setRoleName(roleName);
        }

        for (Role role : roleRepository.findAll()) {
            try {
                // 比對newRole的身分是否已經存在於DB
                if (role.getRoleName().name().equals(roleName.name())) {
                    // 若DB已經有此role，則直接取用DB資料
                    newRole = role;
                    break;
                }
                else {
                    // 若DB無此role，則new一個新物件
                    newRole.setRoleName(roleName);
                }

            }
            catch (IllegalArgumentException e)
            {
                System.out.println("addUserException : " + e.getMessage());
                // 資料庫中出現了Enum中沒有的項目
                // Error from "Roles.valueOf(role.getRoleName().name())"
            }
        }
        // 寫入Role的新物件至DB
        roleRepository.save(newRole);
        return newRole;
    }
}
