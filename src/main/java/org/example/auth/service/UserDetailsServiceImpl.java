package org.example.auth.service;

import org.example.auth.model.Role;
import org.example.auth.model.User;
import org.example.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("Could not find user");
        }

        return new userDetailsImpl(user);

        /*** login successfully but not implement userDetailsImpl (isEnabled not work)***/

//        User user = userRepository.findByUsername(username);
//
//        if (user == null) throw new UsernameNotFoundException(username);
////        user.orElseThrow(() -> new UsernameNotFoundException("Not found : " + username));
//
//        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
//
//        for (Role role : user.getRoles()) {
//            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
//        }
////        if (user.isActive()) {
////        return new userDetailsImpl(user);
//        return new org.springframework.security.core.userdetails.User
//                (user.getUsername(), user.getPassword(), grantedAuthorities);
//        }
//        else {
//            throw new UsernameNotFoundException("username not found");
//        }
//        return user.map(MyUserDetails::new).get();
    }
}
