package org.example.auth.repository;

import org.example.auth.model.User;
import org.example.auth.model.UserUpdate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    /*
    * @Query
    * jpql : SELECT u FROM User(Object name) u WHERE u.username
    * native : value = "SELECT * FROM auth_user(table name) ..." , nativeQuery=true
    * */
//    @Query("SELECT u FROM User u WHERE u.username = :username")
//    public User getUserByUsername(@Param("username") String username);

    User findByUsername(String username);

    @Query("SELECT " +
            "   new org.example.auth.model.UserUpdate(u.id, u.username, u.isActive, r.roleEnum) " +
            "FROM " +
            "   User u " +
            "Join " +
            "   u.roles r " +
            "WHERE " +
            "   u.id = :id")
    UserUpdate findUserInfoById(int id);

//    Optional<User> findByUserName(String userName);

    // failed
    // in JPQL, don't need third(@JoinTable) table in SQL statement
//    @Query("SELECT u.id, u.username, r.name FROM User u join u.roles r")
//    @Query("SELECT u FROM User u join u.roles") // workable
//    @Query("SELECT u, u.roles FROM User u where u member of u.roles") // workable, but will be failed after updating data
//    @Query("FROM User u join u.roles") // workable
//    List<User> queryAllUserRole();


}
