package org.example.auth.validator;

import org.example.auth.service.UserService;
import org.example.auth.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator{
    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        // 不得有空白或文字中有空間(?)
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");

        // Username長度不得小於6 不得大於32
        if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.userForm.username");
        }

        // Username不可和資料庫已有內容重複
        if (userService.findByUsername(user.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }

        // Password長度 不得小於8 大於32
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password");
        }

        // 兩次輸入password內容不符則reject
        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
        }

        // user isActive() == false
//        if (!user.isActive()) {
//            errors.rejectValue("isActive", "Diff.userForm.isActive");
//        }
    }
}
