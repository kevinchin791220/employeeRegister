package org.example.auth.model;

import java.util.List;

public class UserUpdate {
    public int id;
    public String username;
    public boolean isActive;
    public RoleEnum roleEnum;

    public UserUpdate() {
    }

    public UserUpdate(int id, String username, boolean isActive, RoleEnum roleEnum) {
        this.id = id;
        this.username = username;
        this.isActive = isActive;
        this.roleEnum = roleEnum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public RoleEnum getRoleEnum() {
        return roleEnum;
    }

    public void setRoleEnum(RoleEnum roleEnum) {
        this.roleEnum = roleEnum;
    }
}
