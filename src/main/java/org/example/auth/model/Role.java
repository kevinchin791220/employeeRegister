package org.example.auth.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "role")
//@NamedQueries
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    private String name;
    @Enumerated(EnumType.STRING)
    @Column(unique=true)
//    @Column(length = 8)
    private RoleEnum roleEnum;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoleEnum getRoleName() {
        return roleEnum;
    }

    public void setRoleName(RoleEnum roleEnum) {
        this.roleEnum = roleEnum;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
