package org.example.auth.model;

public enum RoleEnum {
    ADMIN,
    CREATOR,
    EDITOR,
    VIEWER;

    RoleEnum() {
    }
}
